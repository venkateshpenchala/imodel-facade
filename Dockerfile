FROM node:12.17.0
ARG NPM_SOURCE=https://registry.npmjs.org/
ARG NPM_USER=info@siemens.com
ARG NPM_PWD=${NPM_PWD}

WORKDIR /app
COPY package.json /app
COPY swagger.json /
RUN echo "_auth = "${NPM_PWD}" email = "{NPM_USER}" always-auth= true" > .npmrc
RUN npm config set registry ${NPM_SOURCE} \
    && npm install --only=prod --no-lockfile    
COPY dist /app
CMD [ "node", "server.js" ]
EXPOSE 3000