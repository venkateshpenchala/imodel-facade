/*---------------------------------------------------------------------------------------------
 * Copyright (c) Siemens AG, 2020. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
import * as express from 'express';
import * as compression from 'compression';  // compresses requests
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as lusca from 'lusca';
import * as dotenv from 'dotenv';
import * as expressValidator from 'express-validator';
import * as bluebird from 'bluebird';
import * as expressJwt from 'express-jwt';
import * as swaggerUI from 'swagger-ui-express';
import * as swaggerDocument from '../swagger.json';
import { SwaggerAPIRouter, ChangesetRouter } from './routes';
import { IModelConfig } from './IModelConfig';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({path: '.env' || '.env.example'});
IModelConfig.setupConfig();

// Create Express server
const app = express();

// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());

app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));

app.use(expressJwt({
    secret: process.env.JWT_SECRET || 'something',
    requestProperty: 'auth',
    credentialsRequired: false,
    getToken: function fromHeader(req: express.Request) {
      const tokenHeader = req.headers.Authorization || req.headers.authorization;
      if (tokenHeader && (tokenHeader as string).split(' ')[0] === 'Bearer') {
        return (tokenHeader as string).split(' ')[1];
      }
    }
  })
    .unless({path: [/\/api-docs\//g, {url: '/', method: 'OPTIONS'}, /\/changesets\//g]})
);

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send({
      msg: 'Invalid or no token supplied',
      code: 401
    });
  }
});

app.use('/iModel/:iModelID/changesets', ChangesetRouter);
/**
 * Add swagger endpoints
 */
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use('/api/v1', SwaggerAPIRouter);

app.use((req: express.Request, resp: express.Response) => {
  resp.status(404).send({
    msg: 'Not Found!'
  });
});

module.exports = app;