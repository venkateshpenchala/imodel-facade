/*---------------------------------------------------------------------------------------------
 * Copyright (c) Siemens AG, 2020. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
import { Router } from 'express';
import ChangeSetController from './controllers/changeset.ctrl';

const ChangesetRouter = Router({ mergeParams: true });
ChangesetRouter.get('/', ChangeSetController.getAll);
export { ChangesetRouter };

const SwaggerAPIRouter = Router();
export { SwaggerAPIRouter };