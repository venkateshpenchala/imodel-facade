/*---------------------------------------------------------------------------------------------
 * Copyright (c) Siemens AG, 2020. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
import { AgentAuthorizationClient, AzureFileHandler, RequestHost } from '@bentley/backend-itwin-client';
import { AuthorizedClientRequestContext } from '@bentley/itwin-client';
import { ContextRegistryClient } from '@bentley/context-registry-client';
import { BriefcaseProvider } from '../utilities/BriefcaseProvider';
import { ChangeSummaryExtractor } from '../utilities/ChangeSummaryExtractor';
import { ChangeSetPostPushEvent, IModelHubClient, IModelQuery, NamedVersionCreatedEvent } from '@bentley/imodelhub-client';
import { AuthorizedBackendRequestContext, BriefcaseDb, BriefcaseManager, IModelHost, IModelHostConfiguration } from '@bentley/imodeljs-backend';
import { Logger } from '@bentley/bentleyjs-core';
import { IModelConfig } from '../IModelConfig';
import { ChangeOpCode } from '@bentley/imodeljs-common';

import * as fs from 'fs';
import * as path from 'path';

/**
 * @class ChangesetService
 */
class ChangesetService {
    private _iModelDb?: BriefcaseDb;
    public constructor(
        private _hubClient: IModelHubClient = new IModelHubClient(new AzureFileHandler()),
        private _connectClient: ContextRegistryClient = new ContextRegistryClient(),
        private _briefcaseProvider: BriefcaseProvider = new BriefcaseProvider(),
        private _changeSummaryExtractor: ChangeSummaryExtractor = new ChangeSummaryExtractor()) {}

  /**
   * @description Fetches specific changeset information
   * @param iModelID
   * @returns {Promise<any>}
   */
  async getallChangesets(iModelID: string): Promise<any> {
    if (!IModelHost.authorizationClient)
      IModelHost.authorizationClient = new AgentAuthorizationClient(IModelConfig.oidcAgentClientConfiguration);
    Logger.logTrace(IModelConfig.loggingCategory, 'Getting changesets');
    const requestContext = await AuthorizedBackendRequestContext.create();
    requestContext.enter();
    const changesets = await this._hubClient!.changeSets.get(requestContext, iModelID);
    changesets.forEach((element) => {
      Logger.logTrace(IModelConfig.loggingCategory, 'the id is' + element.id!);
    });
    return changesets;
  }
}

export default new ChangesetService();