/*---------------------------------------------------------------------------------------------
 * Copyright (c) Siemens AG, 2020. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
import { Request, Response } from 'express';
import { default as ChangesetService } from '../services/changeset.srvc';

class ChangeSetController {
  async getAll(req: Request, resp: Response) {
    try {
      const changesets = await ChangesetService.getallChangesets(req.params.iModelID);
      resp.status(200).send(changesets);
    } catch (error) {
      resp.send({
        msg: 'Not found',
        status: 404
      });
    }
  }
}

export default new ChangeSetController();